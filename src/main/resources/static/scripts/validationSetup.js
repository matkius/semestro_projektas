jQuery.validator.setDefaults({
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback col-sm-4');
        element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
    
});
jQuery.validator.methods.email = function( value, element ) {
    return this.optional( element ) || /^[^@]+@[^\.]+\..+$/.test( value );
  }
  