  function deleteUser(id){
    if (confirm('Are you sure you want to delete this user?')){
      $.ajax({
      type : "DELETE",
      url : "/admin/user/delete/"+id,
      timeout : 100000}).
      done(function(data) {
          console.log("ERROR: ", data.message);
          if(data.message == "success"){
            location.reload();
          }
          else{
            $("#globalError").show().text(data.message);
            console.log("ERROR: ", data.message);
          }
      });
    }
  }