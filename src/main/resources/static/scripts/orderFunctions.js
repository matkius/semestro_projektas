function changeStatus(id, status){
    alert("Changed order status to '"+status+"'");
    $.ajax({
    type : "POST",
    url : "/order/updateStatus/"+id+"/"+status,
    timeout : 100000,
    success : function(id) {
        console.log("SUCCESS: ", id);
        location.reload();
        //alert(response);
    },
    error : function(e) {
        console.log("ERROR: ", e);
        location.reload();
        //alert(e.responseText)
    },
    done : function(e) {
        console.log("DONE");
    }
    });
  }

  function deleteOrder(id){
    if (confirm('Are you sure you want to delete this order?')){
      $.ajax({
      type : "DELETE",
      url : "/order/deleteById/"+id,
      timeout : 100000,
      success : function(id) {
          console.log("SUCCESS: ", id);
          location.reload();
      },
      error : function(e) {
          console.log("ERROR: ", e);
          //alert(e.responseText)
          location.reload();
      },
      done : function(e) {
          console.log("DONE");
      }
      });
    }
  }

  function deleteOrderInDetailsPage(id){
      if (confirm('Are you sure you want to delete this order?')){
        $.ajax({
        type : "DELETE",
        url : "/order/deleteById/"+id,
        timeout : 100000,
        success : function(id) {
            console.log("SUCCESS: ", id);
            window.location.href = "/";
        },
        error : function(e) {
            console.log("ERROR: ", e);
            //alert(e.responseText)
            window.location.href = "/";
        },
        done : function(e) {
            console.log("DONE");
        }
        });
      }
  }