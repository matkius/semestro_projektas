package com.spproject.spproject.controllers;

import com.spproject.spproject.domain.User;
import com.spproject.spproject.domain.UserDto;
import com.spproject.spproject.domain.UserUpdateDto;
import com.spproject.spproject.services.CustomUser;
import com.spproject.spproject.services.UserService;
import com.spproject.spproject.tools.GenericResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    UserService userService;

    @GetMapping(value="/user/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ModelAndView userList() {
        List<User> users = userService.findAll();
        ModelAndView mv = new ModelAndView();
        mv.setViewName("admin/user/all");
        mv.addObject("users",users);
        return mv;
    }

    @GetMapping(value="/user/id/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ModelAndView getByUserId(@PathVariable(name="userId", required = true) Long userId){
        Optional<User> userOptional = userService.findById(userId);
        User user = userOptional.get();
        ModelAndView mv = new ModelAndView();
        mv.setViewName("admin/user/update");
        mv.addObject("user",user);
        return mv;
    }
    @Transactional
    @RequestMapping(value="/user/delete/{id}", method = RequestMethod.DELETE)
    public GenericResponse deleteById(@PathVariable(name="id", required = true) long id){
        return userService.deleteById(id);
    }


    @PostMapping(value = "/user/create", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public GenericResponse userCreate(UserDto userDto) {
        return userService.registerNewUserAccount(userDto, true);
    }

    @PostMapping(value = "/user/updateUser", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public GenericResponse updateUser(UserUpdateDto userDto) {
        return userService.updateUser(userDto);
    }

}