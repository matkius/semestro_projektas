package com.spproject.spproject.controllers;

import com.spproject.spproject.domain.UserDto;
import com.spproject.spproject.services.CustomUser;
import com.spproject.spproject.services.TokenService;
import com.spproject.spproject.services.UserService;
import com.spproject.spproject.tools.GenericResponse;

import javax.servlet.ServletContext;

import com.spproject.spproject.domain.ChangePassDto;
import com.spproject.spproject.domain.ForgotPassChangeDto;
import com.spproject.spproject.domain.ForgotPassRequestDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

@RestController
public class SecurityController {

    Logger logger;
    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private ServletContext servletContext;

    public SecurityController() {
        super();
        logger = LogManager.getLogger(SecurityController.class);
    }

    @PostMapping(value = "/security/login/rest/registration", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public GenericResponse registerUserAccount(UserDto dto) {
        logger.info("Registering user account with information: {}", dto);
        return userService.registerNewUserAccount(dto, false);
    }   

    @PostMapping(value = "/security/userSettings/rest/changePassword", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public GenericResponse changePassword(ChangePassDto dto) {
        CustomUser User = (CustomUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        dto.setId(User.getId());
        logger.info("Changing password with information: {}", dto);
        return userService.changePassword(dto);
    }

    @GetMapping(value = "/security/rest/emailExists")
    public String emailExists(@Param("email") String email) {
        logger.info(email);
        if (userService.emailExists(email)) {
            return "false";
        }
        return "true";
    }

    @GetMapping(value = "/security/rest/emailNotExists")
    public String emailNotExists(@RequestParam(name = "username", required = false) String username,
        @RequestParam(name = "email", required = false) String email) {
        String requestEmail = (username != null) ? username : email;
        logger.info(requestEmail);
        if (userService.emailExists(requestEmail)) {
            return "true";
        }
        return "false";
    }

    @PostMapping(value = "/security/forgotPassword/rest/request", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public GenericResponse forgotPaswordRequest(ForgotPassRequestDto dto) {
        logger.info("Requesting forgot pasword with information: {}", dto);
        return tokenService.CreateForgotToken(dto);
    }

    @PostMapping(value = "/security/forgotPassword/rest/changePassword", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public GenericResponse forgotPaswordChange(ForgotPassChangeDto dto) {
        logger.info("Changing forgoten password with information: {}", dto);
        return tokenService.ChangePassWithToken(dto);
    }

    @GetMapping(value = "/security/login/validateEmail")
    public RedirectView ValidateEmail(@RequestParam(name = "token") String token){
        boolean isGoodToken = tokenService.ValidateEmail(token);
        String path = servletContext.getContextPath();
        return new RedirectView(String.format("%s%s", path, isGoodToken ? "/security/login/succesEmailValidation": "/security/login/unsuccesEmailValidation"));
    }
}
