package com.spproject.spproject.controllers;

import com.spproject.spproject.domain.Category;
import com.spproject.spproject.domain.Order;
import com.spproject.spproject.domain.OrderStatus;
import com.spproject.spproject.services.CustomUser;
import com.spproject.spproject.services.OrderService;
import com.spproject.spproject.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService service;
    @Autowired
    private UserService userService;


    @GetMapping(value="/all")
    public ModelAndView ordersList(Map<String, Object> model) {
        List<Order> orders= service.findAll();
        return collect("order/orders","orders",orders,"All orders");
    }

    @PreAuthorize("#userId == authentication.principal.id")
    @GetMapping(value="/user/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ModelAndView findByUserId(@PathVariable("userId") long userId){
        List<Order> orders= service.findByUserId(userId);
        return collect("order/orders","orders",orders,"Your orders");
    }

    @GetMapping(value="/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ModelAndView findById(@PathVariable(name="id", required = true) int id){
        Order order=  service.findById(id);
//        System.out.println("orderis: " + order.getComments());
        return collect("order/order","order",order,"Order with id: "+id);
    }
    @GetMapping(value="/status/{status}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ModelAndView findById(@PathVariable(name="status", required = true) OrderStatus status){
        List<Order> orders=  service.findByStatus(status);
        return collect("order/orders","orders",orders,"Orders list");
    }

    @RequestMapping(value="/deleteById/{id}", method = RequestMethod.DELETE)
    public String deleteById(@PathVariable(name="id", required = true) int id){
        service.deleteById(id);
        return "redirect:/order/all";
    }

    @GetMapping("/new")
    public String createOrder(){
        return "order/create";
    }

    private ModelAndView collect(String view, String name, Object object, String msg){
        ModelAndView mv = new ModelAndView();
        mv.setViewName(view);
        mv.addObject(name,object);
        mv.addObject("pageName",msg);
        return mv;
    }

    @RequestMapping(value="/filter", produces = MediaType.APPLICATION_JSON_VALUE)
    public ModelAndView findBy(@RequestParam("byWhat") String byWhat,@RequestParam("value") String value,
                               @RequestParam("pageName") String name,@RequestParam("user") long userId) throws ParseException {
        System.out.println("useris: " + userId);
        List<Order> orders;
        try{
            switch(byWhat){
                case "status":
                    OrderStatus status = OrderStatus.valueOf(value.toLowerCase());
                    System.out.println("status: " + status);
                    if(userId==0)
                        orders=service.findByStatus(status);
                    else
                        orders=service.findByStatusByUser(status,userId);
                    break;
                case "category":
                    Category category = Category.valueOf(value.toLowerCase());
                    System.out.println("cat: " + category);
                    if(userId==0)
                        orders=service.findByCategory(category);
                    else
                        orders=service.findByCategoryByUser(category, userId);
                    break;
                case "price-more":
                    if(userId==0)
                        orders=service.findByPriceMore(Double.parseDouble(value));
                    else
                        orders=service.findByPriceMoreByUser(Double.parseDouble(value), userId);
                    break;
                case "price-less":
                    if(userId==0)
                        orders=service.findByPriceLess(Double.parseDouble(value));
                    else
                        orders=service.findByPriceLessByUser(Double.parseDouble(value), userId);
                    break;
                case "date":
                    Date d=new SimpleDateFormat("yyyy-MM-dd").parse(value);
                    System.out.println("date: " + d);
                    if(userId==0)
                        orders=service.findByDate(d);
                    else
                        orders=service.findByDateByUser(d, userId);
                    break;
                default:
                    orders=null;
                    break;
            }
        }catch (Exception e){
            System.out.println("error: " + e);
            return collect("order/orders","orders",null, "All orders");
        }
        return collect("order/orders","orders",orders,name);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String submit(@RequestParam Map<String, String> datamap) {
        service.createOrder(datamap);

        CustomUser customUser = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        long id = customUser.getId();
        return "redirect:/order/user/"+id;
    }


    @RequestMapping(value = "/updateStatus/{id}/{status}", method = RequestMethod.POST)
    public String updateStatus(@PathVariable(name="id", required = true) int id, @PathVariable(name="status", required = true) OrderStatus status) {
        if(status == OrderStatus.accepted) //changing status from available to accepted
            service.acceptOrder(id,status);
        else if (status == OrderStatus.available) //changing status from accepted to available
            service.undoAccept(id, status);
        else //changing into any other status
            service.updateStatus(id,status);

        return "redirect:/order/all";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateOrder(@RequestParam Map<String, String> datamap) {
        Order order= service.findById(Integer.parseInt(datamap.get("id")));
        order.setDestination((String) datamap.get("destination"));
        order.setOrigin((String) datamap.get("origin"));
        order.setWeight(Double.parseDouble( datamap.get("weight")));
        order.setPrice(Double.parseDouble( datamap.get("price")));
        String dimensions= datamap.get("dim1")+"x"+datamap.get("dim2")+"x"+( datamap.get("dim3"));
        order.setDimensions(dimensions);
        Category category = Category.valueOf(datamap.get("category"));
        order.setCategory(category);

        service.save(order);
        CustomUser customUser = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        long id = customUser.getId();
        return "redirect:/order/user/"+id;
    }

}

