package com.spproject.spproject.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.stereotype.Controller;

@Controller
public class HomeController {
    @GetMapping("/")
    public String homePage(){
        return "index";
    }


   // @GetMapping("/allUsers")
   // public String allUsers(){ return "user"; }

}