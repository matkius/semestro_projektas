package com.spproject.spproject.controllers;

import com.spproject.spproject.domain.Comment;
import com.spproject.spproject.domain.Order;
import com.spproject.spproject.domain.User;
import com.spproject.spproject.services.CommentService;
import com.spproject.spproject.services.OrderService;
import com.spproject.spproject.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping("/comment")
public class CommentController {
    @Autowired
    private CommentService service;
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserService userService;

//    @PreAuthorize("#datamap.get('user_id') == authentication.principal.id")
    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String submit(@RequestParam Map<String, String> datamap ) {
        Comment comment= new Comment();
        comment.setComment( datamap.get("comment"));
        long a= Long.parseLong(datamap.get("user_id"));
        int b = Integer.parseInt( datamap.get("order_id"));
        User user= userService.findByUserId(a);
        Order order = orderService.findById(b);
        comment.setUser( user);
        comment.setOrder( order);
        service.save(comment);
        int id=Integer.parseInt( datamap.get("order_id"));
        return "redirect:/order/id/"+id;
    }

    @RequestMapping(value="/remove/{id}", method = RequestMethod.GET)
    public String deleteById(@PathVariable(name="id", required = true) int id, HttpServletRequest request){
        service.deleteById(id);
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

}
