package com.spproject.spproject.error;

public final class UserDoesntExistException extends RuntimeException {

    private static final long serialVersionUID = 45448561241845641L;

    public UserDoesntExistException() {
        super();
    }

}