package com.spproject.spproject.error;

public final class WrongPasswordException extends RuntimeException {

    private static final long serialVersionUID = 5487468876767812L;

    public WrongPasswordException() {
        super();
    }

}