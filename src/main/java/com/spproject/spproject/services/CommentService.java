package com.spproject.spproject.services;

import com.spproject.spproject.domain.Comment;
import com.spproject.spproject.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentService {
    @Autowired
    private CommentRepository repo;
    public void save(Comment comment) {
        repo.save(comment);

    }
    public List<Comment> findByUserId(int orderId){
        List<Comment> comments= new ArrayList<>();
        repo.findByUserId(orderId).forEach(comment->{
            comments.add(comment);
        });

        return comments;
    }

    public void deleteById(int id){
        repo.deleteById(id);
    }

}
