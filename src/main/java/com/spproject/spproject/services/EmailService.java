package com.spproject.spproject.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailService {
  
    @Autowired
    public JavaMailSender emailSender;

    private String websiteAdress = "http://localhost:8081/";
 
    public void sendSimpleMessage(
      String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage(); 
        message.setTo(to); 
        message.setSubject(subject); 
        message.setText(text);
        emailSender.send(message);
    }

    public void sendForgotPaswordMessage(String to, String tokenID){
      String text = String.format("Recovery link:\n%s%s%s\n",websiteAdress, "security/forgotPassword/changePassword?token=", tokenID); 
      sendSimpleMessage(to, "Password recovery", text);
    }

    public void sendEmailValidationMessage(String to, String tokenID){
      String text = String.format("E-mail validation link:\n%s%s%s\n",websiteAdress, "security/login/validateEmail?token=", tokenID); 
      sendSimpleMessage(to, "Email validation", text);
    }
}