package com.spproject.spproject.services;

import com.spproject.spproject.domain.*;
import com.spproject.spproject.tools.GenericResponse;
import com.spproject.spproject.repositories.CommentRepository;
import com.spproject.spproject.repositories.OrderRepository;
import com.spproject.spproject.repositories.UserRepository;
import org.hibernate.usertype.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserService{
    @Autowired
    private UserRepository repo;

    @Autowired
    private OrderRepository orderRepo;

    @Autowired
    private CommentRepository commentRepo;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final String EMAIL_PATTERN = "[^@]+@[^\\.]+\\..+";
    private Pattern pattern = Pattern.compile(EMAIL_PATTERN);
    private Matcher matcher;

    public GenericResponse registerNewUserAccount(UserDto dto, boolean isAdmin){
        matcher = pattern.matcher(dto.getEmail());
        if (dto.isAnyNull() || isAdmin && dto.getPassword() == "") {
            return new GenericResponse("Not all fields are filled", "formError");
        }
        if (!matcher.matches()) {
            return new GenericResponse("Bad email", "emailError");
        }
        if (!dto.getPassword().equals(dto.getMatchingPassword())) {
            return new GenericResponse("Passwords doesn't match", "passwordError");
        }
        if (emailExists(dto.getEmail())) {
            return new GenericResponse("User already exist", "emailError");
        }
        User user = new User();
        user.setUsername(dto.getName());
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setEmail(dto.getEmail());
        if(dto.getRole().equals("courier")){
            user.setRoles(Arrays.asList(Role.USER, Role.COURIER));
        }
        else if(dto.getRole().equals("dispatcher")){
            user.setRoles(Arrays.asList(Role.USER, Role.DISPATCHER));
        }
        else if(dto.getRole().equals("admin") && isAdmin){
            user.setRoles(Arrays.asList(Role.USER, Role.ADMIN));
        }
        if(!isAdmin){
            repo.save(user);
            tokenService.CreateEmailValidationToken(user);
        }
        else{
            user.setVerifiedEmail(true);
            repo.save(user);
        }
        return new GenericResponse("success");
    }

    public GenericResponse changePassword(ChangePassDto dto){
        if (dto.isAnyNull()) {
            return new GenericResponse("All fields aren't filled");
        }
        if (!dto.getPassword().equals(dto.getMatchingPassword())) {
            return new GenericResponse("New passwords doesn't match", "passwordError");
        }
        Optional<User> user = repo.findById(dto.getId());
        if (user.isEmpty()) {
            return new GenericResponse("User doesn't exists", "userError");
        }
        if(!passwordEncoder.matches(dto.getOldPassword(), user.get().getPassword())){
            return new GenericResponse("Wrong password", "passwordError");
        }
        user.get().setPassword(passwordEncoder.encode(dto.getPassword()));
        save(user.get());
        return new GenericResponse("success");
    }

    public boolean emailExists(String email) {
        return repo.findByEmail(email) != null;
    }

    public List<User> findAll() {
        final List<User> users = new ArrayList<>();
        repo.findAll().forEach(user -> {
            users.add(user);
        });
        return users;
    }

    public Optional<User> findById(final Long userId) {
        if (repo.existsById(userId))
            return repo.findById(userId);
        else
            return Optional.empty();
    }
    public User findByUserId(long userId) {
            return repo.findById(userId);
    }

    public List<User> findByType(final String type) {
        final List<User> users = new ArrayList<>();
        repo.findAllByType(type).forEach(user -> {
            users.add(user);
        });
        return users;
    }

    public User findByEmail(String email) {
        return repo.findByEmail(email);
    }

    public void save(final User user) {
        repo.save(user);
    }

    public GenericResponse updateUser(UserUpdateDto dto){
        matcher = pattern.matcher(dto.getEmail());
        if (dto.getName() == "" || dto.getEmail() == "") {
            return new GenericResponse("Not all fields are filled", "formError");
        }
        if (!matcher.matches()) {
            return new GenericResponse("Bad email", "emailError");
        }
        if (!dto.getPassword().equals(dto.getMatchingPassword())) {
            return new GenericResponse("Passwords doesn't match", "passwordError");
        }
        User user = repo.findById(dto.getId());
        if(user == null){
            return new GenericResponse("User doesn't exists", "userError");
        }
        user.setUsername(dto.getName());
        if(dto.getPassword() != "")
            user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setEmail(dto.getEmail());
        if(dto.getRole().equals("courier")){
            user.setRoles(Arrays.asList(Role.USER, Role.COURIER));
        }
        else if(dto.getRole().equals("dispatcher")){
            user.setRoles(Arrays.asList(Role.USER, Role.DISPATCHER));
        }
        else if(dto.getRole().equals("admin")){
            user.setRoles(Arrays.asList(Role.USER, Role.ADMIN));
        }
        repo.flush();
        return new GenericResponse("success");
    }

    public GenericResponse deleteById(final Long id) {
        Optional<User> user = repo.findById(id);
        if (user.isPresent())
        {
            List<Order> orders = new ArrayList<Order>();
            orderRepo.findByDispatcherId(id).forEach(order->{
                orders.add(order);
            });
            orderRepo.findByCourierId(id).forEach(order->{
                orders.add(order);
            });
            if(orders.size() != 0){
                return new GenericResponse("User have orders", "userError");
            }
            tokenService.deleteUserTokens(user.get());
            commentRepo.deleteByUserId(id);
            repo.delete(user.get());
            return new GenericResponse("success");
        }
        return new GenericResponse("User doesn't exists", "userError");
    }


}