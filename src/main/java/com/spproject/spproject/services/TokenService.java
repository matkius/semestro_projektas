package com.spproject.spproject.services;

import java.util.Optional;

import com.spproject.spproject.repositories.TokenRepository;
import com.spproject.spproject.tools.GenericResponse;
import com.spproject.spproject.domain.ForgotPassChangeDto;
import com.spproject.spproject.domain.ForgotPassRequestDto;
import com.spproject.spproject.domain.Token;
import com.spproject.spproject.domain.TokenType;
import com.spproject.spproject.domain.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class TokenService {
    @Autowired
    private TokenRepository repo;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public GenericResponse CreateForgotToken(ForgotPassRequestDto dto){
        if(dto.isAnyNull()){
            return new GenericResponse("All fields aren't filled");
        }
        User user = userService.findByEmail(dto.getEmail());
        if(user == null){
            return new GenericResponse("User doesn't exists", "userError");
        }
        Token token = new Token();
        token.setType(TokenType.FORGOT_PASSWORD);
        token.setUser(user);
        repo.saveAndFlush(token);

        emailService.sendForgotPaswordMessage(dto.getEmail(), token.getId());
        return new GenericResponse("success");
    }

    public GenericResponse ChangePassWithToken(ForgotPassChangeDto dto){
        if(dto.isAnyNull()){
            return new GenericResponse("All fields aren't filled");
        }
        if(!dto.getPassword().equals(dto.getMatchingPassword())){
            return new GenericResponse("Passwords doesnt match", "passwordError");
        }
        Optional<Token> token = repo.findById(dto.getToken());
        if(token.isEmpty()){
            return new GenericResponse("Token doesn't exists", "tokenError");
        }
        if(token.get().getType()!= TokenType.FORGOT_PASSWORD){
            return new GenericResponse("Token doesn't exists", "tokenError");
        }
        User user = token.get().getUser();
        if(user == null){
            return new GenericResponse("User no longer exists", "userError");
        }
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        userService.save(user);
        repo.delete(token.get());
        return new GenericResponse("success");
    }

    public void CreateEmailValidationToken(User user){
        Token token = new Token();
        token.setType(TokenType.FORGOT_PASSWORD);
        token.setUser(user);
        repo.saveAndFlush(token);
        emailService.sendEmailValidationMessage(user.getEmail(), token.getId());
    }

	public Boolean ValidateEmail(String tokenId) {
        Optional<Token> token = repo.findById(tokenId);
        if(token.isEmpty()){
            return false;
        }
        if(token.get().getType()!= TokenType.FORGOT_PASSWORD){
            return false;
        }
        User user = token.get().getUser();
        if(user == null){
            return false;
        }
        user.setVerifiedEmail(true);
        userService.save(user);
        repo.delete(token.get());
        return true;
    }
    
    public void deleteUserTokens(User user){
        repo.deleteByUser(user);
    }
}