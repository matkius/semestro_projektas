package com.spproject.spproject.services;

import com.spproject.spproject.domain.Category;
import com.spproject.spproject.domain.Order;
import com.spproject.spproject.domain.OrderStatus;
import com.spproject.spproject.domain.User;
import com.spproject.spproject.repositories.CommentRepository;
import com.spproject.spproject.repositories.OrderRepository;
import com.spproject.spproject.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class OrderService {
    @Autowired
    private OrderRepository repo;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private CommentRepository commentRepo;

    public List<Order> findAll(){
        List<Order> orders= new ArrayList<>();
        repo.findByOrderByStatusAsc().forEach(order->{
            orders.add(order);
        });
        return orders;
    }
    public Order findById(int id){
        return repo.findById(id);
    }

    public List<Order> findByUserId(long userId){
        List<Order> orders= new ArrayList<>();
        repo.findByDispatcherId(userId).forEach(order->{
            orders.add(order);
        });
        repo.findByCourierId(userId).forEach(order->{
            orders.add(order);
        });
        return orders;
    }
    public List<Order> findByPriceMore(double price){
        List<Order> orders= new ArrayList<>();
        repo.findByPriceGreaterThan(price).forEach(order->{
            orders.add(order);
        });
        return orders;
    }
    public List<Order> findByPriceLess(double price){
        List<Order> orders= new ArrayList<>();
        repo.findByPriceLessThan(price).forEach(order->{
            orders.add(order);
        });
        return orders;
    }
    public List<Order> findByStatus(OrderStatus status){
        List<Order> orders= new ArrayList<>();
        repo.findByStatus(status).forEach(order->{
            orders.add(order);
        });
        return orders;
    }
    public List<Order> findByCategory(Category category){
        List<Order> orders= new ArrayList<>();
        repo.findByCategory(category).forEach(order->{
            orders.add(order);
        });
        return orders;
    }

    public List<Order> findByDate(Date date){
        List<Order> orders= new ArrayList<>();
        repo.findByCreatedAfter(date).forEach(order->{
            orders.add(order);
        });
        return orders;
    }
    public List<Order> findByDateByUser(Date date,long uerId){
        List<Order> orders= new ArrayList<>();
        repo.findByCreatedAfterAndUser(uerId,date).forEach(order->{
            orders.add(order);
        });
        return orders;
    }

    public List<Order> findByStatusByUser(OrderStatus status,long userId){
        List<Order> orders= new ArrayList<>();
        repo.findByStatusAndUser(userId,status.toString()).forEach(order->{
            orders.add(order);
        });
        System.out.println(orders);
        return orders;
    }
    public List<Order> findByCategoryByUser(Category category,long userId){
        List<Order> orders= new ArrayList<>();
        repo.findByCategoryAndUser(userId,category.toString()).forEach(order->{
            orders.add(order);
        });
        return orders;
    }
    public List<Order> findByPriceLessByUser(double price, long userId){
        List<Order> orders= new ArrayList<>();
        repo.findByPriceLessThanAndUser(userId,price).forEach(order->{
            orders.add(order);
        });
        return orders;
    }
    public List<Order> findByPriceMoreByUser(double price, long userId){
        List<Order> orders= new ArrayList<>();
        repo.findByPriceMoreThanAndUser(userId,price).forEach(order->{
            orders.add(order);
        });
        return orders;
    }

    public void deleteById(int id){
        commentRepo.deleteByOrderId(id);
        repo.deleteById(id);
    }

    public void save(Order item) {
        repo.save(item);
    }

    public void createOrder(Map<String, String> datamap){
        Order order= new Order();
        order.setDestination((String) datamap.get("destination"));
        order.setOrigin((String) datamap.get("origin"));
        order.setWeight(Double.parseDouble( datamap.get("weight")));
        order.setPrice(Double.parseDouble( datamap.get("price")));
        String dimensions= datamap.get("dim1")+"x"+datamap.get("dim2")+"x"+( datamap.get("dim3"));
        order.setDimensions(dimensions);
        Category category = Category.valueOf(datamap.get("category"));
        order.setCategory(category);


        CustomUser customUser = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> user = Optional.ofNullable(userRepo.findById(customUser.getId()));
        order.setDispatcher(user.get());

        repo.save(order);
    }

    public void updateStatus(int id, OrderStatus status){
        Order order = repo.findById(id);
        order.setStatus(status);
        repo.save(order);
    }

    public void acceptOrder(int id, OrderStatus status){
        Order order = repo.findById(id);
        CustomUser customUser = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> user = Optional.ofNullable(userRepo.findById(customUser.getId()));
        order.setCourier(user.get());
        order.setStatus(status);
        repo.save(order);
    }

    public void undoAccept(int id, OrderStatus status){
        Order order = repo.findById(id);
        order.setStatus(status);
        order.setCourier(null);
        repo.save(order);
    }







}