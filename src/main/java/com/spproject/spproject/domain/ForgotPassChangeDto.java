package com.spproject.spproject.domain;

public class ForgotPassChangeDto {
    private String token;;

    private String password;

    private String matchingPassword;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public Boolean isAnyNull(){
        return token == null || password == null || matchingPassword == null;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("ForgotPassRequestDto [token=").append(token).append("]");
        return builder.toString();
    }

}