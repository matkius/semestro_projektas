package com.spproject.spproject.domain;

public class ForgotPassRequestDto {
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public Boolean isAnyNull(){
        return email == null;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("ForgotPassRequestDto [email=").append(email).append("]");
        return builder.toString();
    }

}