package com.spproject.spproject.domain;

public class UserUpdateDto {

    private long id;

    private String name;

    private String password;

    private String matchingPassword;

    private String email;

    private String role;

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(final String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public Boolean isAnyNull(){
        return name == null || email == null || password == null || matchingPassword == null || role == null;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("UserDto [name=").append(name).append("]");
        return builder.toString();
    }

}