package com.spproject.spproject.domain;

public enum Role {
    USER, ADMIN, DISPATCHER, COURIER
}