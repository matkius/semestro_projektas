package com.spproject.spproject.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="Orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private double weight;
    private double price;
    private String dimensions;
    private String origin;
    private String destination;
    private Date created = new Timestamp(Calendar.getInstance().getTime().getTime());

    @ManyToOne
    @JoinColumn(name = "dispatcher_id")
    @JsonIgnore
    private User dispatcher;


    @ManyToOne
    @JoinColumn(name = "courier_id")
    @JsonIgnore
    private User courier;

    @Enumerated(EnumType.STRING)
    @Column(name="status")
    private OrderStatus status = OrderStatus.available;

    @Enumerated(EnumType.STRING)
    @Column(name="category")
    private Category category;

    @OneToMany(
            mappedBy = "order",
            cascade = CascadeType.PERSIST,
            fetch = FetchType.LAZY
    )
    private List<Comment> comments;


    public Order(){

    }
    public Order(String origin, String destination){
        this.origin=origin;
        this.dimensions=dimensions;
    }

    public int getId() {
        return id;
    }
    public String getDimensions() {
        return dimensions;
    }

    public OrderStatus getStatus() {
        return status;
    }
    public Category getCategory() {
        return category;
    }
    public double getPrice(){return price;}

    public void setCategory(Category category) { this.category = category; }
    public void setStatus(OrderStatus status) {
        this.status = status;
    }
    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }
    public double getWeight() {
        return weight;
    }
    public Date getCreated() {
        return created;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    public String getOrigin() {
        return origin;
    }
    public List<Comment> getComments() {
        return comments;
    }
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public User getCourier() {
        return courier;
    }

    public void setCourier(User courier) {
        this.courier = courier;
    }

    public User getDispatcher() {
        return dispatcher;
    }

    public void setDispatcher(User dispatcher) {
        this.dispatcher = dispatcher;
    }
}