package com.spproject.spproject.domain;

public enum TokenType {
    FORGOT_PASSWORD, ACTIVATE_USER
}