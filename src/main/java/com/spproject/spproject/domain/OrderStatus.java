package com.spproject.spproject.domain;

public enum OrderStatus {
    available,accepted, rejected, inTransit, delivered,failedToDeliver
}
