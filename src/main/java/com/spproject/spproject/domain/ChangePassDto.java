package com.spproject.spproject.domain;

public class ChangePassDto {
    private Long id;

    private String oldPassword;

    private String password;

    private String matchingPassword;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(final String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    
    public Boolean isAnyNull(){
        return id == null || oldPassword == null || password == null || matchingPassword == null;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("ChangePassDto [email=").append(id).append("....]");
        return builder.toString();
    }

}