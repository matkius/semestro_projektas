package com.spproject.spproject.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@Entity
@Table(name="Comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String comment;
    private Date created = new Timestamp(Calendar.getInstance().getTime().getTime());

    @OneToOne
    @JoinColumn(name = "order_id")
    @JsonIgnore
    private Order order;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    public Comment(){

    }
    public User getUser() {
        return user;
    }
    public int getId() {
        return id;
    }
    public String getComment() { return comment;}
    public Order getOrder() {
        return order;
    }


    public void setUser(User user) {
        this.user = user;
    }
    public void setComment(String comment) {this.comment = comment;}
    public void setOrder(Order order) {this.order = order; }
    public String getCreated() {
        String pattern = "dd-M-yyyy hh:mm";
        SimpleDateFormat simpleDateFormat =new SimpleDateFormat(pattern, new Locale("lt", "LT"));
        return simpleDateFormat.format(created);
    }
}
