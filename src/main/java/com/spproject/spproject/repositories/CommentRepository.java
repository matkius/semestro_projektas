package com.spproject.spproject.repositories;

import com.spproject.spproject.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {
    public Comment save(Comment comment);
    public List<Comment> findByUserId(long id);
    public void deleteById(int id);

    @Transactional
    public void deleteByOrderId(int id);

    @Transactional
    public void deleteByUserId(long id);
}
