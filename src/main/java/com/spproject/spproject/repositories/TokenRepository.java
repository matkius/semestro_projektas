package com.spproject.spproject.repositories;

import com.spproject.spproject.domain.Token;
import com.spproject.spproject.domain.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenRepository extends JpaRepository<Token, String> {
    public void deleteByUser(User user);
}