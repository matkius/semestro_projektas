package com.spproject.spproject.repositories;

import com.spproject.spproject.domain.Category;
import com.spproject.spproject.domain.Order;
import com.spproject.spproject.domain.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer>
{
    public List<Order> findByOrderByStatusAsc();
    public Order findById(int id);
    public List<Order> findByCourierId(long id);
    public List<Order> findByDispatcherId(long id);
    public List<Order> findByStatus(OrderStatus status);

    public List<Order> findByCategory(Category category);
    public List<Order> findByCreatedAfter(Date date);
    public List<Order> findByPriceGreaterThan(double price);
    public List<Order> findByPriceLessThan(double price);
    public Order save(Order order);
    public void deleteById(int id);
    public long count();

    @Query(value="SELECT * FROM `orders` o WHERE (o.courier_id = ?1 or o.dispatcher_id = ?1) and o.status=?2",nativeQuery = true)
    public List<Order> findByStatusAndUser(long userId, String stats);

    @Query(value="SELECT * FROM `orders` o WHERE (o.courier_id = ?1 or o.dispatcher_id = ?1) and o.category=?2",nativeQuery = true)
    public List<Order> findByCategoryAndUser(long userId, String cat);

    @Query(value="SELECT * FROM `orders` o WHERE (o.courier_id = ?1 or o.dispatcher_id = ?1) and o.price < ?2",nativeQuery = true)
    public List<Order> findByPriceLessThanAndUser(long userId, double p);

    @Query(value="SELECT * FROM `orders` o WHERE (o.courier_id = ?1 or o.dispatcher_id = ?1) and o.price > ?2",nativeQuery = true)
    public List<Order> findByPriceMoreThanAndUser(long userId, double p);


    @Query(value="SELECT * FROM `orders` o WHERE (o.courier_id = ?1 or o.dispatcher_id = ?1) and o.created >= ?2",nativeQuery = true)
    public List<Order> findByCreatedAfterAndUser(long userId, Date d);





//    @Query(value = "SELECT * FROM orders u WHERE u.status = :status and u.name = :name",nativeQuery = true)
//    User findUserByStatusAndNameNamedParamsNative(@Param("status") Integer status, @Param("name") String name);
}
