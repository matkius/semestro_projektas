package com.spproject.spproject.repositories;

import com.spproject.spproject.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT * FROM users WHERE type = ?1 ", nativeQuery = true)
    public List<User> findAllByType(String type);

    User findByEmail(String email);
    public User findById(long id);
    @Override
    void delete(User user);
}