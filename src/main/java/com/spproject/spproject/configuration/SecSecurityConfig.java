package com.spproject.spproject.configuration;

import com.spproject.spproject.services.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@ComponentScan(basePackages = { "com.spproject.spproject" })
@EnableWebSecurity
@EnableAutoConfiguration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecSecurityConfig extends WebSecurityConfigurerAdapter {
    private static final String ROLE_USER = "USER";
    private static final String ROLE_DISPATCHER = "DISPATCHER";
    private static final String ROLE_ADMIN = "ADMIN";
    private static final String ROLE_COURIER = "COURIER";

    @Autowired
    private MyUserDetailsService userDetailsService;

    public SecSecurityConfig() {
        super();
    }

    // @Bean
    // @Override
    // public AuthenticationManager authenticationManagerBean() throws Exception {
    // return super.authenticationManagerBean();
    // }

    @Override
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**","/scripts/**", "/css/**", "/static/**", "/favicon.ico", "/images/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
        .csrf().disable()//unblocks post requests
        .authorizeRequests()
            .antMatchers("/", "/security/rest/**").permitAll()
            .antMatchers("/security/login/**", "/security/forgotPassword/**").anonymous()
            .antMatchers("/logout", "/security/userSettings/**").hasAuthority(ROLE_USER)
            .antMatchers( "/order/filter","/order/user/**", "/order/id/**", "/comment/create", "/comment/remove/**").hasAnyAuthority(ROLE_COURIER, ROLE_DISPATCHER, ROLE_ADMIN)
                .antMatchers("/order/form", "/order/new", "/order/create","/order/update", "/order/deleteById/**").hasAnyAuthority(ROLE_DISPATCHER, ROLE_ADMIN)
            .antMatchers("/order/status/available", "/order/updateStatus/**").hasAnyAuthority(ROLE_COURIER, ROLE_ADMIN)
            .antMatchers("/admin/order/all", "/admin/user/all", "/admin/user/new").hasAnyAuthority(ROLE_ADMIN)
            .anyRequest().hasAuthority(ROLE_ADMIN)
        .and()
        .formLogin()
            .loginPage("/security/login/login")
            .defaultSuccessUrl("/")
            .permitAll()
        .and()
        .logout()
			.permitAll();
        // @formatter:on
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}